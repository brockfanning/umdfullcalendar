(function ($) {
    Drupal.behaviors.my_umd_calendar_behavior = {
        attach: function(context, settings) {

            $('.fullcalendar').fullCalendar({
                events: '/my-ajax-calendar-callback'
            });
        }
    }
})(jQuery);