## Steps if not using FullCalendar module

* Use hook_menu() to expose a path for your calendar
* At that path, include the FullCalendar js, css, and a custom js file
* The custom js file starts FullCalendar and specifies a path for AJAX calls
* Use hook_menu() to expose this path for AJAX calls
* At that path, query your events and output raw data in JSON format


Installation:

Downloads:

Drupal modules
drush dl ctools views date

Gotcha, use dev version of fullcalendar module:
drush dl fullcalendar-7.x-2.x-dev

FullCalendar library: (use the last 1.5.x version)
https://github.com/arshaw/fullcalendar/releases/download/v1.5.4/fullcalendar-1.5.4.zip

Gotcha: The fullcalendar module will DELETE an existing library when it's enabled

Setup:
drush site-install
drush upwd admin --password="admin123"
drush en fullcalendar views_ui

Gotcha:
Turn off "Automatically update preview on changes" at /admin/structure/views/settings

Content prep:

1. Create an event content type
2. Add a date field with start/end dates
3. drush en devel_generate
4. Generate a lot of events

Configuration:

1. Create a view with FullCalendar style, no pager, no limit
2. Add the date field
3. Turn on AJAX

Demo:

Refer back to "Why Not Calendar module?" slide and see how it compares

1. Drag-drop, show logging out
2. Show how to disable it in Views settings
3. Talk about other settings
4. Enable FullCalendar Options, talk about extra options
5. Enable Colorbox, talk about that option
6. Enable Colors and FullCalendar Color, talk about that options.
7. Enable FullCalendar legend and show that.

## Gotchas

* Use the dev version: 7.x-2.x-dev (the stable version is over 2 years old)
* Use older version of FullCalendar: 1.5
* Remember FullCalendar module will delete the library when you enable it...
* Turn off "Automatically update preview on changes" at /admin/structure/views/settings
* Views configuration: No pager, no limit, and use AJAX

## Why NOT FullCalendar library?

* Need the Year display
* Don't want to require javascript

## Why NOT FullCalendar module?

* Need custom logic surrounding event title/text
* Need to use latest version of FullCalendar
* Need custom markup in the date-boxes (thumbnails, etc)